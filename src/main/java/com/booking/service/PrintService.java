package com.booking.service;

import com.booking.models.Customer;
import com.booking.models.Employee;
import com.booking.models.Reservation;
import com.booking.models.Service;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class PrintService {
    public static void printMenu(String title, String[] menuArr){
        int num = 1;
        System.out.println(title);
        for (int i = 0; i < menuArr.length; i++) {
            if (i == (menuArr.length - 1)) {   
                num = 0;
            }
            System.out.println(num + ". " + menuArr[i]);   
            num++;
        }
    }

    public String printServices(List<Service> serviceList){
        String result = "";
        // Bisa disesuaikan kembali
        for (Service service : serviceList) {
            result += service.getServiceName() + ", ";
        }
        return result;
    }

    // Function yang dibuat hanya sebgai contoh bisa disesuaikan kembali
    public void showRecentReservation(List<Reservation> reservationList){
        int num = 1;
        System.out.printf("| %-4s | %-4s | %-11s | %-15s | %-15s | %-15s | %-10s |\n", "No.", "ID", "Nama Customer", "Service", "Biaya Service", "Pegawai", "Workstage");
        System.out.println("+========================================================================================+");
        for (Reservation reservation : reservationList) {
            if (reservation.getWorkstage().equalsIgnoreCase("Waiting") || reservation.getWorkstage().equalsIgnoreCase("In process")) {
                System.out.printf("| %-4s | %-4s | %-11s | %-15s | %-15s | %-15s | %-10s |\n",
                num, reservation.getReservationId(), reservation.getCustomer().getName(), printServices(reservation.getServices()), reservation.getReservationPrice(), reservation.getEmployee().getName(), reservation.getWorkstage());
                num++;
            }
        }
    }

    public static void showAllCustomer(List<Customer> customers) {
        System.out.printf("| %-4s | %-9s | %-11s | %-15s | %-15s | %-15s |\n", "No.", "ID", "Nama", "Alamat", "Membership", "Uang");
        System.out.println("+========================================================================================+");
        AtomicInteger number = new AtomicInteger(1);
        customers
                .stream()
                .forEach(data -> {
                    System.out.printf("| %-4d | %-9s | %-11s | %-15s | %-15s | %-15s |\n",
                            number.getAndIncrement(), data.getId(), data.getName(),
                            data.getAddress(),
                            data.getMember().getMembershipName(),
                            data.getWallet());
                });
    }


    public static void showAllEmployee(List<Employee> employees){
        System.out.printf("| %-4s | %-9s | %-11s | %-11s | %-15s |\n", "No.", "Id", "Nama", "Alamat", "Pengalaman");
        System.out.println("+========================================================================================+");
        AtomicInteger number = new AtomicInteger();
        employees
                .stream()
                .forEach(data -> {
                    number.incrementAndGet();
                    System.out.printf("| %-4s | %-9s | %-11s | %-11s | %-15s |\n",
                            number, data.getId(), data.getName(),
                            data.getAddress(),
                            data.getExperience());
                });
    }

    public void showHistoryReservation(){
        
    }
}
