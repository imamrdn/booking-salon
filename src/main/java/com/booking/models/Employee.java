package com.booking.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.List;

@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Employee extends Person {
    private int experience;

    public static List<Employee> getAllEmployee(List<Person> personList) {
        return personList
                .stream()
                .filter(Employee.class::isInstance)
                .map(Employee.class::cast)
                .toList();
    }
}
